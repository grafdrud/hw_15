﻿#include <iostream>
#include <string>

void F(const int N, const bool EvenOdd)
{
	for (int b = EvenOdd; b <= N; b += 2)
	{
		std::cout << b << " ";
	}
}

int main()
{
	//Задаём целое числовое значение N к которому будем стремиться, не может быть меньше 'a'
	//Задаём вывод для чётных или нечётных чисел, EvenOdd = 1 - нечётные, 0 - чётные
	std::cout << "Enter a positive number:";
	int N = 0;
	std::cin >> N;

	//a += 2 тоже самое, что a = a + 2
	for (int a = 0; a <= N; a += 2)
	{
		std::cout << a << " ";
	}
	std::cout << "\n";

	std::cout << "Enter 0 for even, or 1 for odd numbers:";
	bool EvenOdd = 0;
	std::cin >> EvenOdd;

	F(N, EvenOdd);
}